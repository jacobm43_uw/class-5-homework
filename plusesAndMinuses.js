// Write JavaScript here
let counter = 0;
const count = document.getElementById('count');
const plusEl = document.getElementById('plus');
// fires first
plusEl.addEventListener('click', function(e) {
  counter++;
  count.innerHTML = counter; 
  console.log(`count is ${counter}`);
  }
);

const minusEl = document.getElementById('minus');
// fires first
minusEl.addEventListener('click', function(e) {
  counter--;
  count.innerHTML = counter; 
  console.log(`count is ${counter}`);
  }
);