// If an li element is clicked, toggle the class "done" on the <li>
const toggleDoneClass = function(e) {
  const old_classname = this.className;
  if (old_classname === 'done'){
    this.className = '';
  } else {
    this.className = 'done';
  }
}

const liEls = document.getElementsByTagName('li');
for (let i=0;i<liEls.length;i++) {
  liEls[i].addEventListener('click', toggleDoneClass);
}

// If a delete link is clicked, delete the li element / remove from the DOM
const delEls = document.getElementsByClassName('delete');
for (let i=0;i<delEls.length;i++) {
  delEls[i].addEventListener('click', function(e) {
    this.parentNode.parentNode.removeChild(this.parentNode);
    } 
  );
}

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const clickToToday = function(e) {
  e.stopPropagation()
  let newParent = document.getElementsByClassName('later-list')[0];
  this.className = 'move toToday';
  this.text = 'Move to Today';
  this.removeEventListener('click', clickToToday);
  this.addEventListener('click', clickToLater);
  newParent.appendChild(this.parentNode);
}
const clickToLater = function(e) {
  e.stopPropagation()
  let newParent = document.getElementsByClassName('today-list')[0];
  this.className = 'move toLater';
  this.text = 'Move to Later';
  this.removeEventListener('click', clickToLater);
  this.addEventListener('click', clickToToday);
  newParent.appendChild(this.parentNode);
}

const movetoLEls = document.getElementsByClassName('move toLater');
for (let i=0;i<movetoLEls.length;i++) {
  movetoLEls[i].addEventListener('click', clickToToday);
}

const movetoTEls = document.getElementsByClassName('move toToday');
for (let i=0;i<movetoTEls.length;i++) {
  movetoTEls[i].addEventListener('click', clickToLater);
}


// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
  e.preventDefault();

  const input = e.target.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>
  
  let newLi = document.createElement('li');
  let newSpan = document.createElement('span');
  const aTextNode = document.createTextNode(text);
  let newDel = document.createElement('a');
  let newMove = document.createElement('a');
  newDel.className = 'delete';
  newDel.text = 'Delete';
  newDel.addEventListener('click', function(e) {
    this.parentNode.parentNode.removeChild(this.parentNode);
    } 
  );
  newSpan.appendChild(aTextNode);
  newLi.appendChild(newSpan);
  newLi.addEventListener('click', toggleDoneClass);

  let newParent = e.target.parentNode.parentNode;
  if (newParent.className === 'today'){
    
    newMove.className = 'move toLater';
    newMove.text = 'Move to Later';
    newMove.addEventListener('click', clickToLater);
    newLi.appendChild(newMove);
    newLi.appendChild(newDel);
    newParent.getElementsByTagName('ul')[0].appendChild(newLi);
  } else {

    newMove.className = 'move toToday';
    newMove.text = 'Move to Today';
    newMove.addEventListener('click', clickToToday);
    newLi.appendChild(newMove);
    newLi.appendChild(newDel);
    newParent.getElementsByTagName('ul')[0].appendChild(newLi);
  }
  const movetoLEls = document.getElementsByClassName('move toLater');
}

// Add this as a listener to the two Add links
const addEls = document.getElementsByClassName('add-item');
for (let i=0;i<addEls.length;i++) {
  addEls[i].addEventListener('click', addListItem);
}