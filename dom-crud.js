// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let mainEl = document.getElementsByTagName('main')[0];

let aEl = document.createElement('a');
aEl.setAttribute('id','cta');
const aTextNode = document.createTextNode('Buy Now!');
aEl.appendChild(aTextNode);
mainEl.appendChild(aEl);

// Access (read) the data-color attribute of the <img>,
// log to the console
let imgEl = document.getElementsByTagName('img')[0];
console.log(imgEl.attributes.getNamedItem('data-color').value);


// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
let turboLiEl = document.getElementsByTagName("li")[2];
turboLiEl.setAttribute('class','highlight');

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
let pList = document.getElementsByTagName('p');
let pEl = pList[ pList.length - 1 ];
pEl.remove();